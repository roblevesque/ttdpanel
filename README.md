# TTD Panel

## About
TTD Panel is a configuration management 'engine' for [TwoToneDetect](https://www.twotonedetect.net/). TwoToneDetect is an application (as per the website): "**A Windows (and Linux) program that detects two-tone pages, records audio, and sends the recording to email or cell phones**". Typically, it is used in a fire service setting for alerting members to a toned out call.

TTD Panel is a tool for managing and generating the configuration for that software in a (hopefully) userfriendly fashion.

TTD Panel is not affiliated in any way with TwoToneDetect or it's creator. Permission to create and distribtue this for free, however, was granted by TwoToneDetect's creator.

## Installation Instructions

### 0. Requirements
- Python 3 (Debian based systems try: `sudo apt install python3`)
- virtualenv (Debian based systems try `sudo apt install virtualenv`)
- Some technical knowledge
- A place to run/host this.
- A TwoToneDetect installation somewhere visible to the above

### 1. Clone repo and install requirements
```
git clone https://gitlab.com/roblevesque/ttdpanel.git
cd ttdpanel
make install
```
- This will pull down the repo, and setup a Python virtualenv for it with all the appropriate dependencies. It will also seed the database with some default carrier data (YMMV on how it'll work for you) and a default admin user of `admin` and default password of `password` that should be promptly changed

### 2. Setup application instance and configure application
```
cp instance_dist instance
<vim/vi/nano/etc> config.cfg
```

- In config.cfg:
  - Modify ENCRYPTION_KEY to your needs, or keep empty if not utilizing the TTD encryption feature. KEYS MUST BE 8 CHARACTERS (A TTD limitation, not a TTDPanel one)
  - Modify SECRET_KEY to something very random and long. Perfect chance to mash the keyboard if you want. Oh and I've been told that changing it may break things. YMMV
  - Change listening IP address if required and port if you want it on the default. It's recommended to change the listening IP to 127.0.0.1 and proxy access to it via nginx or haproxy to restrict access to tones.cfg, however. Directions on how to do this are outside of the scope of this README.
  - This software has usernames and passwords so *I STRONGLY RECCOMEND USING NGINX TO CONFIGURE REVERSE PROXYING AND AN SSL CERTIFICATE* 


## Running Instructions
From top level ttdpanel directory:
`./run`
or
`bash ./run`

I have some a systemd unit file floating around that I may be able to provide, if there is enough interest, and integrate into the Makefile.


## TwoToneDetect setup
You'll need to point TwoToneDetect at this monster by pointing it to whatever host you're running it on.  So something like `http://127.15.138.43:5000/tones.cfg` if you're not proxying it or `https://127.15.138.43/tones.cfg` if you do decide to do that (Do that, really)


## Accessing/Using

### Web Interface
Assuming default configs and no reverse proxy, the web interface will appear on http://hostip:5000/    You should be able to log into the default admin account with the username `admin` and the password `password`.  I would change that as the first thing you do by going to the User's panel and hitting te pencil next to the 'admin' name. 

### CLI

As of this moment there are three things you can do from the command line. They all require that you enter the virtualenv to access them.  
From the main directory run: 
```
source venv/bin/activate
export FLASK_APP=ttdpanel
```

At that point you can run the following commands:
-  `flask reset-pw user newpassword`   This will reset the given user's password to newpassword. Useful if you forget the admin password
-  `flask add-user user email password`  This creates a new user with the given email and password. This may be deprecated now that there is a GUI user manager
-  `flask seed-db` Not very useful outside of initial setup. Seeds DB with admin user and carrier list. Might be useful if you want to modify DB backend. 

`TODO: I need to bake in some secure ways to activate the CLI for backend tasks to be preformed`  



## Known issues
- I haven't run into any yet but as of writing this I made some changes (added user control panel) that may have broken it. Please report back if possible.


## Future features (maybe)

- I may decide to add an 'invite' system where a short, but unique, URL will be generated that members can self enroll into the system. Could include options to auto-moderate the invites, or have approvals be required to be added once signed up. 
- Member login to self-manage contact settings.
- CLI/API hooks to be triggered by TwoToneDetect on various stages of the call. Possible integrations with other systems. Need to think about this. 