"""empty message

Revision ID: 3e871d582283
Revises:51f91c7c215a
Create Date: 2019-10-16 13:30:19.809405

"""
from alembic import op
import sqlalchemy as sa
import sqlalchemy_utils


# revision identifiers, used by Alembic.
revision = '3e871d582283'
down_revision = '51f91c7c215a'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('alert_member',
        sa.Column('email', sa.String(length=120))
    )
    pass


def downgrade():
    pass
