"""empty message

Revision ID: 51f91c7c215a
Revises: 9e01fba0f8e2
Create Date: 2019-10-14 11:24:07.187994

"""
from alembic import op
import sqlalchemy as sa
import sqlalchemy_utils


# revision identifiers, used by Alembic.
revision = '51f91c7c215a'
down_revision = '9e01fba0f8e2'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('triggers',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name',  sa.String(length=180), nullable=False),
    sa.Column('guid', sa.String(length=180), nullable=False),
    sa.Column('settings', sa.Text(), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('guid'),
    sa.UniqueConstraint('name')
    )
    pass


def downgrade():
    drop_table('triggers')
    pass
