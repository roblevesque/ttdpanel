SHELL := /bin/bash
.EXPORT_ALL_VARIABLES:
FLASK_APP=ttdpanel
FLASK_ENV=development
define END_TEXT
Final steps:
    - Change SECRET_KEY in instance/config.cfg to something random. This value should never change after this point. Ever.
    - Change ENCRYPTION_KEY to appropriate value as configured in the TTD config. If its not currently in use, consider using it or just keep it blank
    - Consider putting a proxy (nginx, HAProxy, Apache) of some sort in front of this. If you do that, tweak the LISTEN_ON value to only listen to localhost

endef
export END_TEXT

install: instance/application.db
	source venv/bin/activate; flask seed-db
	@echo "$$END_TEXT"

instance/application.db: configfile
	source venv/bin/activate; flask db upgrade

configfile: requirements
	mkdir -p instance
	cp instance_dist/config.cfg instance/config.cfg

requirements:
ifdef VIRTUAL_ENV
		@echo "Already in virtualenv. Skipping the creation of it."
else
		virtualenv -p python3 venv
endif
	source venv/bin/activate; pip3 install -r requirements.txt



.PHONY: update
update:
	source venv/bin/activate;	flask db upgrade
.PHONY: debug
debug:
	source venv/bin/activate;	flask run --host=0.0.0.0

clean:
	@echo Removing instance folder
	rm -rf instance
	find -iname "*.pyc" -delete

reallyclean: clean
	@echo Disposing of venv directory
	rm -rf venv
