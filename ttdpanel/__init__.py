import os
import re

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
import sqlalchemy_utils
import json
import phonenumbers




def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
            SECRET_KEY = 'dev',
            SQLALCHEMY_DATABASE_URI = os.path.join("file://", app.instance_path, 'testdb.sqlite'),
            SQLALCHEMY_TRACK_MODIFICATIONS = False,
    )

    if test_config is None:
        app.config.from_pyfile('config.cfg', silent=True)
        app.config['SQLALCHEMY_DATABASE_URI'] = app.config['SQLALCHEMY_DATABASE_URI'].format(instance= app.instance_path)
    else:
        app.config.from_mapping(test_config)

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # DB
    from ttdpanel.Models import db
    db.init_app(app)
    migrate = Migrate(app, db)

    from . import admin
    from ttdpanel.admin import perm_check
    # Custom template filers and globals
    app.add_template_global(ttdp_jsonify, name="json_loads")
    app.add_template_global(phone_format, name="phone_format")
    app.add_template_global(perm_check, name="perm_check")
    app.add_template_global(get_scheme, name="get_scheme")
    app.add_template_filter(regex_replace, name="regex_replace")



    # Admin Panel

    from ttdpanel.admin import add_user_command
    from ttdpanel.admin import reset_passwd_command
    from ttdpanel.admin import seed_db_command
    from ttdpanel.triggermanager import execute_trigger_command
    app.register_blueprint(admin.bp)
    app.cli.add_command(add_user_command)
    app.cli.add_command(reset_passwd_command)
    app.cli.add_command(seed_db_command)
    app.cli.add_command(execute_trigger_command)

    # Alert Manager Panel
    from . import alertmanager
    app.register_blueprint(alertmanager.bp)
    app.add_url_rule('/', endpoint='index')

    # Trigger Manager Module
    from . import triggermanager
    app.register_blueprint(triggermanager.bp)


    return app

def regex_replace(s, find, replace):
    """A non-optimal implementation of a regex filter"""
    return re.sub(find, replace, s)


def phone_format(n):
    try:
        return  phonenumbers.format_number(phonenumbers.parse(n,'US'),  phonenumbers.PhoneNumberFormat.NATIONAL)
    except:
        return None

def ttdp_jsonify(n):
    return  json.loads(n)

def get_scheme():
    if request.url.startswith('http://'):
        return "http"
    else:
        return "https"
