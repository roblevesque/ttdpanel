$( document ).ready(function() {
	jQuery.validator.addMethod("valueNotEquals", function(value, element, arg){
 return arg !== value;
}, "Value must not equal blank value.");

	jQuery.validator.addMethod("noSpace", function(value, element)   { //Code used for blank space Validation
	    return value.indexOf(" ") < 0 && value != "";
		}, "No spaces please");


	var mutationObserver = new MutationObserver(function(mutations) {
  mutations.forEach(function(mutation) {
   	$(mutation.addedNodes).each(function() {
				if ($(this).hasClass('flash')) {
						setTimeout(function() { $('.flash').fadeOut('slow') }, 5000)
				}
		})
  });
});
	mutationObserver.observe(document.documentElement, {
	  attributes: true,
	  characterData: true,
	  childList: true,
	  subtree: true,
	  attributeOldValue: true,
	  characterDataOldValue: true
	});
 $(document).find('#member-table').DataTable();
 $(document).find('#user-table').DataTable();
 $(document).find('#trigger-table').DataTable();
 $(document).find('#carrier-table').DataTable();
 $(document).find('#tone-table').DataTable();

	/* Remove tone from member edit screen */
	$(document).on('click','.tone-delete', function() {
		tone_name = $(this).data('tone')
		//$(`[data-tone]==%{tone_name}`).remove();
		$(this).parent().remove();
	})
	/* Change handler for member-tone-add select box */
	$(document).on('change','select[data-tline]',function() {
		line_id = $(this).data('tline')
		$(`input[data-tline=${line_id}`).data('tone',  $(this).children(':selected').text() )
	})
  /* Add new tone line to member edit box */
	$('#member-tone-add').click(function() {
		$.ajax({
			url: "/_tone",
			method: 'GET',
			dataType: 'json',
			contentType: 'application/json',
			data: "",
			success: function(msg) {
					//tonelist = JSON.parse(msg);
					random_id = btoa(Math.random()).substr(5, 5);

						tone = "<div class='row'>";
					  tone += `<div class='col-sm-2'> <select data-tline="${random_id}">`;
						msg.forEach(function(x){
							tone += `<option value="${x.id}" data-tonename="${x.name.trim()}">${x.name.trim()}</option>`;
						});
						tone += `</select></div>`;
						tone += `<div class='col-sm'><input data-tline="${random_id}"  data-type="email" type='checkbox'></div>`;
						tone += `<div class='col-sm'><input data-tline="${random_id}"  data-type="phone" type='checkbox'></div>`;
						tone += `<div class='col-sm'>   </div>`
					  tone += `<div class='col-sm'><input data-tline="${random_id}"  data-type="text" type='checkbox'></div>`;
					  tone += `<div class='col-sm'><input data-tline="${random_id}"  data-type="mp3" type='checkbox'></div>`;
					  tone += `<div class='col-sm'><input data-tline="${random_id}"  data-type="amr" type='checkbox'></div>`;
						tone += `<span class='col-sm text-center fas fa-minus tone-delete' data-tone="" data-tline="${random_id}"></span>`;
				    tone += "</div>"
					$('#member-tone-table').append(tone)
					$(document).find(`:checkbox[data-tline=${random_id}]`).each(function() {
						 $(this).addClass("switch switch-sm");
						 $(this).wrap(`<label class="switch"></span>`).parent().append(`<div class="slider slider-sm round"></div>`);
					 });
					var selected_tone = $(document).find(`select[data-tline=${random_id}]`).children(':selected').text()
					$(document).find(`input[data-tline=${random_id}]`).each(function() {
						$(this).attr('data-tone', selected_tone.trim());
					})
			}
		})

	})


  $(document).on('click', ".remove-member", function() {
		member_id = {id:  $(this).data('id') };
		$.ajax({
			url: "/_member",
			method: 'DELETE',
			dataType: 'json',
			contentType: "application/json",
			data: JSON.stringify( member_id ),
			success: function(msg) {
				  doFlash('Success!')
					$.get(window.location.pathname).done(function(reply) {
						currentpage = $(document).find('#member-table').DataTable().page();
					 	$(document).find('#member-table').DataTable().destroy();
						$("#member-table").replaceWith($(reply).find('#member-table'));
	 					$(document).find('#member-table').DataTable();

						$(document).find('#member-table').DataTable().page(Math.min(currentpage,($(document).find('#member-table').DataTable().page.info().pages - 1) )).draw("page");
					})
			}
		}

		)
	})

$(document).on('click', ".remove-user", function() {
	user_id = {id:  $(this).data('id') };
	$.ajax({
		url: "/_user",
		method: 'DELETE',
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify( user_id ),
		success: function(msg) {
			  doFlash('Success!')
				$.get({url:window.location.pathname, cache:false}).then(function(reply) {
				 	$(document).find('#user-table').DataTable().destroy();
					$("#user-table").replaceWith($(reply).find('#user-table'));
 					$(document).find('#user-table').DataTable();
				})
		}
	}

	)
})

$(document).on('click', ".remove-trigger", function() {
	trigger_id = {id:  $(this).data('id') };
	$.ajax({
		url: "/_trigger",
		method: 'DELETE',
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify( trigger_id ),
		success: function(msg) {
			  doFlash('Success!')
				$.get({url:window.location.pathname, cache:false}).then(function(reply) {
				 	$(document).find('#trigger-table').DataTable().destroy();
					$("#trigger-table").replaceWith($(reply).find('#trigger-table'));
 					$(document).find('#trigger-table').DataTable();
				})
		}
	}

	)
})

$(document).on('click', ".test-trigger", function() {
	trigger_id = $(this).data('id')
	trigger_url =  $($(".trigger-url").filter( `[data-id="${trigger_id}"]` )[0]).val()

	$.ajax({
		url: trigger_url,
		method: 'GET',
		dataType: 'json',
		contentType: "application/json",
		success: function(msg) {
			  doFlash('Success!')
		}
		})
})

$(document).on('click', ".copy-trigger-url", function() {
	trigger_id = $(this).data('id')
	trigger_url_object = $(".trigger-url").filter( `[data-id="${trigger_id}"]` )[0]
	$(trigger_url_object).css({'display' : 'inherit'});
	$(trigger_url_object).select();
	document.execCommand("copy");
	$(trigger_url_object).css({'display' : ''})
	})


	$('#editMemberModalSave').click(function() {
		// Build tone data object
		var tones = {};
		$('input[data-tone]').each(function() {

			tone = $(this).data('tone');
			type = $(this).data('type');
			tones[tone] = tones[tone] || {}
			tones[tone][type]= $(this).is(':checked')
		});

		// Build user data array for JSON conversion.
		var member_data ={}
		member_data.id = $('#member-id').val();
		member_data.name = $('#member-name').val();
		member_data.phone = $('#phone-number').val();
		member_data.carrier_id = $('#carrier option:selected').val();
		member_data.receiveops = tones;
		member_data.email = $('#email-address').val();
		// Submit data via ajax to backend
		$.ajax({
			url: "/_member",
			type: 'POST',
			dataType: 'json',
			contentType: 'application/json',
			data:  JSON.stringify( member_data ),
			success: function(msg) {
				doFlash('Success!')
				$('#editMemberModal').modal('toggle');
				$.get(window.location.pathname).done(function(reply) {
						$("#member-table").DataTable().destroy()
						$("#member-table").replaceWith($(reply).find('#member-table'))
						$(document).find("#member-table").DataTable();
				})
			},
			error:function(jqXHR) {
				console.log("Update error: " + jqXHR.status);
			doFlash('Error!')
				$('#editMemberModal').modal('toggle');
				cleanupMemberEditModal();
			}
		});

	});

	/* Field population on member edit modal */
	$('#editMemberModal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget); // Button that triggered the modal
  	var member = button.data('id'); // Extract info from data-* attributes
		var modal = $(this);
		if (member != undefined) {
		$.ajax({
			url: "/_member",
			type: 'GET',
			data: {id : member },
			dataType: 'json',
			contentType: 'application/json',
			success: function(msg) {
				modal.find('.modal-title').text('Edit Member: ' + msg.name);
				modal.find('#member-id').val(member);
		  	modal.find('#member-name').val(msg.name);
				modal.find('#phone-number').val(msg.phone);
				modal.find('#email-address').val(msg.email);
				modal.find('#carrier option[value=' + msg.carrier.id + ']').prop('selected', true);
				tones = "";
				tonelist=JSON.parse(msg.receiveops);
				 for (var tone in tonelist) {
				  tones += "<div class='row'>"
				  tones += `<div class='col-sm-2'><label for='${tone}'>${tone}</label></div>`
					tones += ( `<div class='col-sm'><input data-tone="${tone}" data-type="email" type='checkbox'`  + ( tonelist[tone].email ? 'checked':'')  + ' ></div>' )
					tones += ( `<div class='col-sm'><input data-tone="${tone}" data-type="phone" type='checkbox'`  + ( tonelist[tone].phone ? 'checked':'')  + ' ></div>' )
					tones += ( `<div class='col-sm'> </div>` )
				  tones += ( `<div class='col-sm'><input data-tone="${tone}" data-type="text" type='checkbox'`  + ( tonelist[tone].text ? 'checked':'')  + ' ></div>' )
				  tones += ( `<div class='col-sm'><input data-tone="${tone}" data-type="mp3" type='checkbox'`  + ( tonelist[tone].mp3 ? 'checked':'')  + ' ></div>' )
				  tones += ( `<div class='col-sm'><input data-tone="${tone}" data-type="amr" type='checkbox'`  + ( tonelist[tone].amr ? 'checked':'')  + ' ></div>' )
					tones += ( `<span class='col-sm text-center fas fa-minus tone-delete' data-tone="${tone}" ></span>` )
			    tones += "</div>"
				}
				if ( tones == "" ) { $('#member-tone-table').html("");  } else {
					$('#member-tone-table').html(tones);
					$(document).find(".modal").find(":checkbox").each(function() {
						 $(this).addClass("switch switch-sm");
						 $(this).wrap(`<label class="switch"></span>`).parent().append(`<div class="slider slider-sm round"></div>`);
					 });
				}
			}
		});
	} else {
			cleanupMemberEditModal()
			modal.find('.modal-title').text("Adding new member");
			modal.find('#member-id').val("-1");
			modal.find('#member-name').val("");
			modal.find('#phone-number').val("");
			$('#member-tone-table').html("");
	}

	});

/* Trigger edit modal save. */
	$('#editTriggerModalSave').click(function() {
		if($(this.form).valid()) {

			// Build trigger settings data
			settings = {}
			$('#trigger-settings-wrapper input, #trigger-settings-wrapper select').each(function() {
				if ($(this).is("select")) {
					settings[$(this).data('setting')] = $(this).children(':selected').val();
				}
				else {
					if ( $(this).attr('type') == 'number'  ){
						settings[$(this).data('setting')] = parseInt( $(this).val() );
					}
					else {
						settings[$(this).data('setting')] = $(this).val();
					}
				}
			})

			// Instantiate trigger object to popoulate with data
			trigger = {};
			trigger.id = $("#trigger-id").val();
			trigger.name = $("#trigger-name").val();
			settings['type'] = $("#trigger-type").children(':selected').val();
			trigger.settings = settings;


			// Submit data via ajax to backend
			$.ajax({
				url: "/_trigger",
				type: 'POST',
				dataType: 'json',
				contentType: 'application/json',
				data:  JSON.stringify( trigger ),
				success: function(msg) {
					doFlash('Success!')
					$('#editTriggerModal').modal('toggle');
					$.get({url: window.location.pathname, cache:false}).then(function(reply) {
							$("#trigger-table").DataTable().destroy()
							$("#trigger-table").replaceWith($(reply).find('#trigger-table'))
							$(document).find("#trigger-table").DataTable();
					})
				},
				error:function(jqXHR) {
					console.log("Update error: " + jqXHR.status);
				doFlash('Error!')
					$('#editTriggerModal').modal('toggle');
					cleanupTriggerEditModal();
				}
			});

		}
	});

/* User edit modal save. */
	$('#editUserModalSave').click(function() {
		if($(this.form).valid()) {
			// Build permission data object
			var perms = {};
			$('input[data-perm]').each(function() {
				perms[$(this).data('perm')] = $(this).is(':checked');
			});

			// Build user data array for JSON conversion.
			var user_data ={}
			user_data.id = $('#user-id').val();
			user_data.username = $('#username').val();
			user_data.email = $('#email').val();
			user_data.perms = perms;
			user_data.password = $('#password').val();
			user_data.confirmpassword = $('#confirmpassword').val();

			// Submit data via ajax to backend
			$.ajax({
				url: "/_user",
				type: 'POST',
				dataType: 'json',
				contentType: 'application/json',
				data:  JSON.stringify( user_data ),
				success: function(msg) {
					doFlash('Success!')
					$('#editUserModal').modal('toggle');
					$.get({url: window.location.pathname, cache:false}).then(function(reply) {
							$("#user-table").DataTable().destroy()
							$("#user-table").replaceWith($(reply).find('#user-table'))
							$(document).find("#user-table").DataTable();
					})
				},
				error:function(jqXHR) {
					console.log("Update error: " + jqXHR.status);
				doFlash('Error!')
					$('#editUserModal').modal('toggle');
					cleanupUserEditModal();
				}
			});
		}
	});


	/* Field population on User edit modal */
	$('#editUserModal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget); // Button that triggered the modal
		var user = button.data('id'); // Extract info from data-* attributes
		var modal = $(this);
		cleanupUserEditModal();


		if (user != undefined) {
		$.ajax({
			url: "/_user",
			type: 'GET',
			data: {id : user },
			dataType: 'json',
			contentType: 'application/json',
			success: function(msg) {
				user_perms = JSON.parse( msg.perms )
				modal.find('.modal-title').text('Edit User: ' + msg.username);
				modal.find('#user-id').val(user);
				modal.find('#username').val(msg.username);
				modal.find('#email').val(msg.email);
				modal.find('input[data-type=modperm]').each(function() {
					$(this).prop('checked', user_perms[ $(this).data('perm') ] );
				});
				}
		});
	} else {
			modal.find('.modal-title').text("Adding new user");
			modal.find('#user-id').val("-1");
			modal.find('#username').val("");
			modal.find('#email').val("");
	}
	});


$('#editTriggerModal').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget); // Button that triggered the modal
	var trigger = button.data('id'); // Extract info from data-* attributes
	var modal = $(this);
	cleanupTriggerEditModal();


	if (trigger != undefined) {
		$.ajax({
			url: "/_trigger",
			type: 'GET',
			data: {id : trigger },
			dataType: 'json',
			contentType: 'application/json',
			success: function(msg) {
				modal.find('.modal-title').text('Edit Trigger: ' + msg.name);
				modal.find('#trigger-guid').text(msg.guid)
				modal.find('#trigger-name').val(msg.name);
				settings = JSON.parse(msg.settings);
				modal.find('#trigger-type option[value=' + settings.type + ']').prop('selected', true);
				modal.find('#trigger-type').prop('disabled',true);
				modal.find('#trigger-id').val(trigger);

				/* Populate settings area */
				$.ajax({
					url: "/_triggertypes",
					type: 'GET',
					dataType: 'json',
					contentType: 'application/json',
					success: function(msg) {
						  html = ""
							typesettings = msg[settings.type]['settings'];
							for (s in typesettings) {
								html = `<div class="formgroup">`

								html += `<label for="setting-${s}">${s[0].toUpperCase()+s.substring(1)}</label>`;
								if ( ["list", "tuple"].includes(typesettings[s].type) )  {
									html += `<select class="form-control" id="setting-${s}" name="setting-${s}" data-setting="${s}">`;
									for  (v in typesettings[s]['values']) {
										html += `<option value="${typesettings[s]['values'][v]}">${typesettings[s]['values'][v]}</option>`;
										}
									html += "</select>"
									html += `</div>`
									$('#trigger-settings-wrapper').append(html);
									$(document).find(`#setting-${s} option[value="${settings[s]}"]`).prop('selected',true);
								}
								else {
									switch (typesettings[s].type) {
										case "int":
											inputtype="number"
											break;
										case "str":
											inputtype="text"
											break;
										default:
											inputtype="text"
									}
									html += `<input type="${inputtype}" class="form-control" id="setting-${s}" name="setting-${s}"  data-setting="${s}">`;
									html += `</div>`
									$('#trigger-settings-wrapper').append(html);
									$(document).find(`#setting-${s}`).val(settings[s]);

								}

							}
				   }

		    });
			}
		});
	} else {
		cleanupTriggerEditModal();
		modal.find('.modal-title').text("Adding new trigger");
	}
});




  /* Tone edit save hadler */
	$('#editToneModalSave').click(function() {

			var settings_obj = {};
			 $("#tone-container input").each(function() {
				 if ( $(this).val()  != "" || $(this).attr('type') == 'checkbox' )  {
					 if ( $(this).attr('type') == 'checkbox') {
						 console.log( ($(this).is(':checked') ? 1 : 0 )) ;
						 settings_obj[$(this).attr('id')] = ($(this).is(':checked') ? 1 : 0 );
					 } else {
					 settings_obj[$(this).attr('id')] = $(this).val();
				 }
			 }
			 })

			 triggers_obj = [];
			 $('#trigger-container input').each(function() {
				 if ($(this).is(':checked')) {
					 triggers_obj.push($(this).data('trig'))
				 }

			 tone_obj = {};
			 tone_obj['id'] = $('#tone-id').data('id');
			 tone_obj['settings'] = settings_obj;
			 tone_obj['triggers'] = triggers_obj;


			 })

			 $.ajax({
				 url: "/_tone",
				 type: 'POST',
				 dataType: 'json',
				 contentType: "application/json",
				 data:  JSON.stringify( tone_obj ),
				 success: function(msg) {
					 doFlash('Success!')
					 $.get(window.location.pathname).done(function(reply) {
							 $("#tone-table").replaceWith($(reply).find('#tone-table'))
					 })
					 cleanupToneEditModal();
				 },
				 error:function(jqXHR) {
					 console.log("Update error: " + jqXHR.status);
					 $('.dynamicflash').html('<div class="flash">Error!</div>');
					 cleanupToneEditModal();
				 }
			 });

	});

	/* Field population on tone edit modal */
	$('#editToneModal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget); // Button that triggered the modal
		var tone = button.data('id'); // Extract info from data-* attributes
		var modal = $(this);
		if (tone != undefined) {
		$.ajax({
			url: "/_tone",
			type: 'GET',
			data: {id : tone },
			dataType: 'json',
			success: function(msg) {
				modal.find('.modal-title').text('Edit Tone ' + msg.name);

				/* Injest tonesettings.json */
				var tonesettings = []
				$.getJSON('/static/tonesettings.json', function(data) {
					tonesettings = data;

					/* Build Form */
					var html = "";
					tonetype = msg.name.slice(0,-1);
					current_settings = JSON.parse(msg.settings)
					try {
						current_triggers = JSON.parse(msg.triggers)

					}
					catch {
						current_triggers = []
					}
					for (setting in tonesettings[tonetype]['settings']) {
						setting_values = tonesettings[tonetype]['settings'][setting];
						current_setting = current_settings[setting]
						html += "<div class='form-group row'>";
						html += `<div class="w-50"><label for="${setting}">${setting}</label></div> <div class="w-auto"><input type="${setting_values.inputtype}" value="${  current_setting != undefined ? ($.isNumeric(current_setting)	 ? parseFloat(current_setting).toFixed(2) : current_setting   ) : "" }" id="${setting}"><i>${setting_values['unit']}</i> </div>`;

						html += "</div>";
					}
					$('#tone-container').html(html);
					$('#tone-id').data('id', msg.id)

					/* Settings checkbox handling */
					$(document).find(".modal").find("#tone-container").find(":checkbox").each(function() {
						 $(this).prop('checked', current_settings[$(this).attr('id')] ? true : false);
						 $(this).addClass("switch switch-sm");
						 $(this).wrap(`<label class="switch"></span>`).parent().append(`<div class="slider round"></div>`);
					 });

					$.ajax({
						url: "/_trigger",
						type: 'GET',
						dataType: 'json',
						success: function(msg) {
							triglist = ""
							msg.forEach(function(trig) {
								triglist += `<div class="form-group row"><div class="w-50"><label for="trig-${trig.id}">${trig.name}</label></div> <div class="w-auto"><input type="checkbox" data-trig="${trig.id}" id="trig-${trig.id}" name="trig-${trig.id}"></input> </div></div>`
							});
							$('#trigger-list').html(triglist);

						/* Trigger checkbox handling */
						$(document).find(".modal").find("#trigger-container").find(":checkbox").each(function() {
						 	 $(this).prop('checked', current_triggers.indexOf($(this).data('trig')) >= 0 ? true : false)
							  $(this).addClass("switch switch-sm");
							 $(this).wrap(`<label class="switch"></span>`).parent().append(`<div class="slider round"></div>`);
						 });
				 }
			 });

				});
			}
		});
	} else {

	}

	});

$('#newToneModal').on('show.bs.modal', function (event) {
	cleanupNewToneModal();
	$('#tone-type-select').prop("selectedIndex", 0);
})


  /* New tone save hadler */
	$('#newToneModalSave').click(function() {
		/* Build settings object and tone_blob for flask consumption */
		var settings_obj = {};
		$("#new-tone-container input").each(function() {
			if ( $(this).val()  != "" ) {
				if ( $(this).attr('type') == 'checkbox') {
					settings_obj[$(this).attr('id')] = ($(this).is(':checked') ? 1 : 0 );
				} else {
				settings_obj[$(this).attr('id')] = $(this).val();
				}
			}
		})
		triggers_obj = [];
		$('#newtone-trigger-container input').each(function() {
			if ($(this).is(':checked')) {
				triggers_obj.push($(this).data('trig'))
			}
		});
		var tone_blob = {};
		tone_blob.type = $('#tone-type').data('type');
		tone_blob.settings = settings_obj;
		tone_blob.triggers = triggers_obj;

		$.ajax({
			url: "/_tone",
			type: 'POST',
			dataType: 'json',
			contentType: "application/json;charset=UTF-8",
			data:  JSON.stringify( tone_blob ),
			success: function(msg) {
				doFlash('Success!')
				$.get(window.location.pathname).done(function(reply) {
						$("#tone-table").replaceWith($(reply).find('#tone-table'))
				})
				cleanupNewToneModal();
			},
			error:function(jqXHR) {
				console.log("Update error: " + jqXHR.status);
			  doFlash('Error!')
				cleanupNewToneModal();
			}
		});
	})


/* Field population on change of trigger type */
$("#trigger-type").change(function() {
	$('#trigger-settings-wrapper').html("");
	selectedtype = $(this).children(":selected").val();
	/* Populate settings area */
	$.ajax({
		url: "/_triggertypes",
		type: 'GET',
		dataType: 'json',
		contentType: 'application/json',
		success: function(msg) {
				html = ""
				typesettings = msg[selectedtype]['settings'];
				for (s in typesettings) {
					html = `<div class="formgroup">`

					html += `<label for="setting-${s}">${s[0].toUpperCase()+s.substring(1)}</label>`;
					if ( ["list", "tuple"].includes(typesettings[s].type) )  {
						html += `<select class="form-control" id="setting-${s}" name="setting-${s}" data-setting="${s}">`;
						for  (v in typesettings[s]['values']) {
							html += `<option value="${typesettings[s]['values'][v]}">${typesettings[s]['values'][v]}</option>`;
							}
						html += "</select>"
						html += `</div>`
						$('#trigger-settings-wrapper').append(html);
					}
					else {
						switch (typesettings[s].type) {
							case "int":
								inputtype="number"
								break;
							case "str":
								inputtype="text"
								break;
							default:
								inputtype="text"
						}
						html += `<input type="${inputtype}" class="form-control" id="setting-${s}" name="setting-${s}"  data-setting="${s}">`;
						html += `</div>`
						$('#trigger-settings-wrapper').append(html);
					}

				}
		 }

	});

})

	/* Field population on new tone modal when selectbox is changed */
	$('#tone-type-select').change(function() {
		if ( $(this).children(':selected').val()  != "" ) {
			$("#newToneModalSave").removeAttr('disabled');
			cleanupNewToneModal();

			var tonesettings = []
			var tonetype =  $(this).children(':selected').val()
			$.getJSON('/static/tonesettings.json', function(data) {
				tonesettings = data;

				/* Build Form */
				var html = "<hr>";

				for (setting in tonesettings[tonetype]['settings']) {
					setting_values = tonesettings[tonetype]['settings'][setting];
					html += "<div class='form-group row'>";
					html += `<div class="w-50"><label for="${setting}">${setting}</label></div> <div class="w-auto"><input type="${setting_values.inputtype}" title="${setting_values.description}" id="${setting}"><i>${setting_values['unit']}</i></div>`;
					html += "</div>";
				}

				$('#new-tone-container').html(html);
				$('#tone-type').data('type',tonetype);

				$(document).find(".modal").find(":checkbox").each(function() {
					 $(this).addClass("switch switch-sm");
					 $(this).wrap(`<label class="switch"></span>`).parent().append(`<div class="slider round"></div>`);
				 });

				 $.ajax({
				 	url: "/_trigger",
				 	type: 'GET',
				 	dataType: 'json',
				 	success: function(msg) {
				 		triglist = ""
				 		msg.forEach(function(trig) {
				 		triglist += `<div class="form-group row"><div class="w-50"><label for="trig-${trig.id}">${trig.name}</label></div> <div class="w-auto"><input type="checkbox" data-trig="${trig.id}" id="trig-${trig.id}" name="trig-${trig.id}"></input> </div></div>`
				 		});
						$('#newtone-trigger-container h6').prop('hidden', false)
				 		$('#newtone-trigger-list').html(triglist);
					}});


				 // End
			});

		} else {
			$("#newToneModalSave").attr('disabled','disabled');
			cleanupNewToneModal();
		}

	})


	$(document).on('click', ".remove-tone", function() {
		tone_id = {id:  $(this).data('id') };
		$.ajax({
			url: "/_tone",
			method: 'DELETE',
			dataType: 'json',
			contentType: "application/json;charset=UTF-8",
			data: JSON.stringify( tone_id ),
			success: function(msg) {
			  	doFlash('Success!');
					$.get(window.location.pathname).done(function(reply) {
					$("#tone-table").replaceWith($(reply).find('#tone-table'))
					})
			}
		}

		)
	})

  /* Populate carrier information if id provided. */
	$('#editCarrierModal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget); // Button that triggered the modal
		var carrier = button.data('id'); // Extract info from data-* attributes
		var modal = $(this);
		if (carrier != undefined) {
		$.ajax({
			url: "/_carrier",
			type: 'GET',
			data: {id : carrier },
			dataType: 'json',
			success: function(msg) {
					modal.find('.modal-title').text('Edit Carrier: ' + msg.name);
					modal.find('#carrier-id').val(carrier);
			  	modal.find('#carrier-name').val(msg.name);
					modal.find('#text-address').val(msg.textappend);
					modal.find('#mp3-address').val(msg.mp3append);
					modal.find('#amr-address').val(msg.amrappend);
			}
		});
	} else {
			cleanupEditCarrierModal();
	}
})


$('#editCarrierModalSave').click(function() {
			carrier_blob = {}
			carrier_blob.id = $('#carrier-id').val();
			carrier_blob.name = $('#carrier-name').val();
			carrier_blob.textappend = $('#text-address').val();
			carrier_blob.mp3append = $('#mp3-address').val();
			carrier_blob.amrappend = $('#amr-address').val();

			$.ajax({
				url: "/_carrier",
				type: 'POST',
				dataType: 'json',
				contentType: "application/json",
				data:  JSON.stringify( carrier_blob ),
				success: function(msg) {
					doFlash('Success!')
					$.get(window.location.pathname).done(function(reply) {
							$("#carrier-table").replaceWith($(reply).find('#carrier-table'))
					})
					cleanupEditCarrierModal();
				},
				error:function(jqXHR) {
					console.log("Update error: " + jqXHR.status);
				doFlash('Error!')
					cleanupEditCarrierModal();
				}
			});


	});
$(document).on('click', ".remove-carrier", function() {
	carrier_id = {id:  $(this).data('id') };
	$.ajax({
		url: "/_carrier",
		method: 'DELETE',
		dataType: 'json',
		contentType: "application/json;charset=UTF-8",
		data: JSON.stringify( carrier_id ),
		success: function(msg) {
				doFlash('Success!');
				$.get(window.location.pathname).done(function(reply) {
				$("#carrier-table").replaceWith($(reply).find('#carrier-table'))
				})
		}
	}

	)
});



$("#triggerform").validate({
	rules: {
		name:{
			required: true
		},
		type: {
			valueNotEquals: "blank"
		}

	},
	messages: {
		name: "Please provide a valid  (non blank) trigger name",
		type: "Please select a trigger type option"
	},
	submitHandler: function(form) {
		form.submit();
	}
});
$("#edit-profile").validate({
	rules: {
		email: {
			required: true,
			email: true
		},
		password: {
			required: function(element){
				return $("#confirmpassword").val().length > 0;
			},
			minlength: 5
		},
		confirmpassword: {
			required:function(element){
				return $("#password").val().length > 0;
			},
			minlength: 5,
			equalTo: "#password"
		}
	},
	messages: {
		username: "Please provide a valid username (No spaces)",
		email: "Please enter an email address",
		confirmpassword: "Passwords do not match"
	},
	submitHandler: function(form) {
		form.submit();
	}

});
$("#userform").validate({
			rules: {
				username: {
					required : true,
					noSpace: true
				},
				email: {
					required: true,
					email: true
				},
				password: {
					required: function(element){
            return $("#confirmpassword").val().length > 0;
          },
					minlength: 5
				},
				confirmpassword: {
					required:function(element){
            return $("#password").val().length > 0;
          },
					minlength: 5,
					equalTo: "#password"
				}
			},
			messages: {
				username: "Please provide a valid username (No spaces)",
				email: "Please enter an email address"
			},
			submitHandler: function(form) {
				form.submit();
			}
});

function cleanupUserEditModal() {
		$('#user-id').val("-1");
		$('#username').val("");
		$('#email').val("");
		$('input[data-perm]').prop('checked', false);
		$('input[type=password]').each(function() { $(this).val('') });
	}
function cleanupTriggerEditModal() {
		$('#trigger-id').val("-1");
		$('#trigger-name').val("");
		$('#trigger-settings-wrapper').html("")
		$('#trigger-guid').html("")
		$('#trigger-type').prop("selectedIndex", 0);
		$('#trigger-type').prop("disabled", false);

	}
function cleanupMemberEditModal() {
	$('#member-id').val("-1");
	$('#member-name').val("");
	$('#phone-number').val("");
	$('#email-address').val("");
	$('#member-tone-table').html("");
}
function cleanupNewToneModal() {
	$('#new-tone-container').html("");
	$('#tone-type').data('type','');
	$('#newtone-trigger-container h6').prop('hidden', true)
	$('#newtone-trigger-list').html("")
}
function cleanupToneEditModal() {
	$('#tone-container').html("");
	$('#tone-id').val("");
	$('#trigger-list').val("")
}
function cleanupEditCarrierModal() {
	$('.modal-title').text('Create New Carrier');
	$('#carrier-id').val("");
	$('#carrier-name').val("");
	$('#text-address').val("");
	$('#mp3-address').val("");
	$('#amr-address').val("");
}

function doFlash(msg) { // giggity
	$('.dynamic-flash').html(`<div class="flash w-100 ">${msg}</div>`);
}



});
