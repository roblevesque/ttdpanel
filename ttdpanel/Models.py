import click
from flask import Flask
from flask import current_app, g
from flask_sqlalchemy import SQLAlchemy
import sqlalchemy_utils
from sqlalchemy_utils import PasswordType, force_auto_coercion
from flask.cli import with_appcontext
import json
from sqlalchemy.types import JSON


db = SQLAlchemy()

def to_json_ready(inst, cls):
    """
    Jsonify the sql alchemy query result.
    """
    convert = dict()
    # add your coversions for things like datetime's
    # and what-not that aren't serializable.
    d = dict()
    for c in cls.__table__.columns:
        v = getattr(inst, c.name)
        if c.type in convert.keys() and v is not None:
            try:
                d[c.name] = convert[c.type](v)
            except:
                d[c.name] = "Error:  Failed to covert using ", str(convert[c.type])
        elif v is None:
            d[c.name] = str()
        else:
            d[c.name] = v
    return d

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(90), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(PasswordType(
        schemes=[
            'pbkdf2_sha512',
            'md5_crypt'
            ],
            deprecated=['md5_crypt']
        ),
        unique=False,
        nullable=False
    )
    perms = db.Column(db.Text)
    @property
    def jsonready(self):
        return to_json_ready(self, self.__class__)

class Triggers(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(180), unique=False, nullable=False)
    guid = db.Column(db.String(180), unique=False, nullable=False)
    settings = db.Column(db.Text)
    lastuse = db.Column(db.DateTime)

    @property
    def jsonready(self):
        return to_json_ready(self, self.__class__)


class AlertMember(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(180), unique=False, nullable=True)
    phone = db.Column(db.String(15), unique=False, nullable=True)
    carrier_id = db.Column(db.Integer, db.ForeignKey('carrier.id'))
    carrier = db.relationship("Carrier")
    receiveops = db.Column(db.Text)
    email = db.Column(db.String(120))

    @property
    def jsonready(self):
        return to_json_ready(self, self.__class__)


class Carrier(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(180), unique=True, nullable=False)
    textappend = db.Column(db.String(255), nullable=False)
    amrappend = db.Column(db.String(255), nullable=False)
    mp3append = db.Column(db.String(255), nullable=False)

    @property
    def jsonready(self):
        return to_json_ready(self, self.__class__)
    def __getitem__(self, item):
        return getattr(self, item)


class Tone(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(180), unique=True, nullable=False)
    settings = db.Column(db.Text)
    triggers = db.Column(db.Text)

    @property
    def jsonready(self):
        return to_json_ready(self, self.__class__)
