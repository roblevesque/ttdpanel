import functools
import click

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for, current_app, Flask
)
from flask.cli import with_appcontext
from ttdpanel.Models import User, Carrier
from ttdpanel.Models import db
import json

bp = Blueprint('admin', __name__, url_prefix='/admin')

@bp.route('/login', methods=('GET', 'POST'))
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        error = None
        try:
            user = User.query.filter(User.username.ilike(username)).one()
            if user.password != password:
                raise "Invalid username or password."
        except:
            user = None
        if user is None:
            error = 'Invalid username or password.'
        if error is None:
            session.clear()
            session['user_id'] = user.id
            return redirect(url_for('index'))

        flash(error)
    return render_template('auth/login.html')


@bp.before_app_request
def load_logged_in_user():
    user_id = session.get('user_id')

    if user_id is None:
        g.user = None
    else:
        g.user = User.query.filter_by(id=user_id).one()

@bp.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('index'))

def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('admin.login'))
        return view(**kwargs)
    return wrapped_view

@bp.route('/profile', methods=['GET', 'POST'])
@login_required
def profile():
    if request.method == 'POST':
        try:
            user_data = request.form
            user = User.query.filter_by(id=g.user.id).one()

            if(user_data.get('password')):
                if (user_data.get('password') == user_data.get('confirmpassword')):
                    user.password = user_data.get('password')
                else:
                    raise Exception("Passwords do not match")
            user.email = user_data.get('email')
            db.session.add(user)
            db.session.commit()
        except Exception as e:
            flash(e.args[0])
        else:
            flash('Profile updated')

    return render_template('alertmanager/profile.html', user=g.user)


def perm_required(perm):
    def decorator(view):
        def wrapped_view(*args, **kwargs):
            if g.user is None:
                return redirect(url_for('admin.login'))

            if ( perm_check(g.user.id, perm) ):
                return view(**kwargs)
            return redirect(url_for('alertmanager.dashboard'))
            return wrapped_view
        return functools.update_wrapper(wrapped_view, view)
    return decorator

def perm_check(user_id, perm):
    if user_id is None:
        return False
    else:
        user = User.query.filter_by(id=user_id).one()
        perms = json.loads(user.perms)
        if ( isinstance(perm, (str) ) ):
            if (perm in perms) and (perms[perm] is True):
                return True
        elif ( isinstance(perm, (list, tuple) ) ):
            for x in perm:
                if (x in perms) and (perms[x] is True):
                    return True
            return False

@click.command('add-user')
@click.argument('user')
@click.argument('emailad')
@click.argument('passwd')
@with_appcontext
def add_user_command(user,emailad,passwd):
    db.init_app(current_app)
    db.session.add(User(username=user, email=emailad, password=passwd))
    db.session.commit()
    click.echo("User added")

@click.command('reset-pw')
@click.argument('user')
@click.argument('passwd')
@with_appcontext
def reset_passwd_command(user,passwd):
    db.init_app(current_app)
    person =  User.query.filter(User.username.ilike( user.encode('utf-8') )).one()
    if person is None:
        person = User.query.filter(User.email.ilike( user.encode('utf-8') )).one()
        if person is None:
            click.echo("Cannot find user")
            return 1
    person.password = passwd
    db.session.commit()
    click.echo("Password reset")

@click.command('seed-db')
@with_appcontext
def seed_db_command():
    db.init_app(current_app)
    if( User.query.count() < 1 ):
        db.session.add(User(username="admin", email="user@example.com", password="password", perms=str('{"modify_members": true, "modify_tones":true, "modify_carriers": true, "modify_users": true, "modify_triggers": true}')))
        db.session.commit()
        click.echo("Default User added")
    if( Carrier.query.count() < 1 ):
        carriers=[
            Carrier(name="Verizon", textappend="vztext.com", amrappend="vzwpix.com", mp3append="vzwpix.com"),
            Carrier(name="AT&T", textappend="txt.att.net", amrappend="mms.att.net", mp3append="mms.att.net"),
            Carrier(name="T-Mobile", textappend="tmomail.net", amrappend="tmomail.net", mp3append="tmomail.net"),
            Carrier(name="Sprint", textappend="messaging.sprintpcs.com", amrappend="pm.sprint.com", mp3append="pm.sprint.com"),
            Carrier(name="Metro PCS", textappend="mymetropcs.com", amrappend="mymetropcs.com", mp3append="mymetropcs.com"),
            Carrier(name="Virgin Mobile", textappend="vmobl.com", amrappend="vmobl.com", mp3append="vmobl.com")
        ]
        db.session.bulk_save_objects(carriers)
        db.session.commit()
