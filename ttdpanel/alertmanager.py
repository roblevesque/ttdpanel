import functools
import click
import os.path
import configparser
try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for, current_app, Flask, jsonify, Response
)
from flask.cli import with_appcontext
from ttdpanel.Models import User
from ttdpanel.Models import AlertMember, Tone, Carrier, Triggers, db
from ttdpanel.admin import login_required
from ttdpanel.admin import perm_required
from flask import abort
import pyDes
import phonenumbers
import json

bp = Blueprint('alertmanager', __name__)

@bp.route('/')
@login_required
def dashboard():
    tones_count = Tone.query.count()
    user_count = User.query.count()
    member_count = AlertMember.query.count()
    trigger_count = Triggers.query.count()
    carrier_count = Carrier.query.count()
    return render_template('alertmanager/home.html', tones_count=tones_count, user_count=user_count, member_count=member_count,trigger_count=trigger_count,carrier_count=carrier_count)


@bp.route('/carrier')
@login_required
@perm_required('modify_carriers')
def carrier_manager():
    carriers = Carrier.query.all()
    return render_template('alertmanager/carriers.html', carriers=carriers)

@bp.route('/tones')
@login_required
@perm_required('modify_tones')
def tone_manager():
    tones = Tone.query.all()
    temp = get_file('static/tonesettings.json').decode('utf-8')
    triggers = Triggers.query.all()
    return render_template('alertmanager/tones.html', tones=tones, types=json.loads(temp), triggers=triggers)


@bp.route('/members')
@login_required
@perm_required('modify_members')
def member_manager():
   members = AlertMember.query.all()
   carriers = Carrier.query.all()
   return render_template('alertmanager/members.html', members=members, carriers=carriers)

@bp.route('/users')
@login_required
@perm_required('modify_users')
def user_manager():
    users = User.query.all()
    return render_template('alertmanager/users.html', users=users)

@bp.route('/tones.cfg')
def output_tone_cfg():
    config_output = StringIO()
    config = configparser.ConfigParser()
    carriers = Carrier.query.all()

    for tone in Tone.query.all():
        list = {}
        email_members = {}
        config.add_section(tone.name)
        settings = json.loads(tone.settings)
        if tone.triggers is not None:
            if current_app.config['TTD_ALERTCMD'] is not None:
                config.set(tone.name, "alert_command", "{} {}".format(current_app.config['TTD_ALERTCMD'], tone.name) )
        for setting in settings:
            config.set(tone.name,setting,"{}".format(settings[setting]))
        for member in  find_members_by_tone("{}".format(tone.name)):
            options = json.loads(member.receiveops)
            for mem_tone in options:
                for sub in options[mem_tone]:
                    if options[mem_tone][sub]:
                        if sub == 'email' or sub == 'phone':
                            continue
                        carrier_blob = member.carrier
                        # Dirty hack to get email addresses in
                        if options[mem_tone].get('email'):
                            email_members[member.id] = { 'name': member.name, 'email': member.email }
                            try:
                                list[sub.encode('utf-8')][member.id] = {'name': member.name, 'phone': None, 'address': None}
                            except:
                                list[sub.encode('utf-8')] = {}
                                list[sub.encode('utf-8')][member.id] = {'name': member.name, 'phone': None, 'address': None}

                        if options[mem_tone].get('phone'):
                                try:
                                    list[sub.encode('utf-8')][member.id] = {'name': member.name, 'phone': member.phone, 'address': carrier_blob.amrappend}
                                except:
                                    list[sub.encode('utf-8')] = {}
                                    list[sub.encode('utf-8')][member.id] = {'name': member.name, 'phone': member.phone, 'address': carrier_blob.amrappend}

        for method in list:
            line = ", "
            temp=[]
            for idx,member in enumerate(list[method]):
                if list[method][member].get('phone') != None :
                    temp.append("\"%s\"<%s@%s>" % (list[method][member]['name'],list[method][member]['phone'], list[method][member]['address']))
                # Dirty hack for email addresses
                if member in email_members:
                    if email_members[member]['email']:
                        temp.append("\"{name}\"<{email}>".format(name=email_members[member]['name'], email=email_members[member]['email'] ) )

            line = line.join(temp)
            config.set(tone.name,"{}_emails".format(method.decode('utf-8')), line)


    config.write(config_output)
    if ('ENCRYPTION_KEY' in current_app.config) & (current_app.config['ENCRYPTION_KEY'] != ""):
        d = pyDes.des(current_app.config['ENCRYPTION_KEY'].encode(),pyDes.ECB, pad='\r', padmode=pyDes.PAD_NORMAL)
        encrypted = d.encrypt(config_output.getvalue().encode())
        return Response(encrypted, mimetype='text/plain')
    else:
        return Response(config_output.getvalue(), mimetype='text/plain')


@bp.route('/_carrier', methods=['GET'])
@login_required
@perm_required(['modify_carriers',"modify_members", "modify_tones"])
def json_get_carrier():
    id = request.args.get('id', -1, type=int)
    if id == -1:
        returnable = []
        for x in Carrier.query.all():
            returnable.append(x.jsonready)
        return jsonify(returnable)
    else:
        try:
            tone = Carrier.query.filter(Carrier.id==id).one()
        except:
            return jsonify({'status': 'Failure'}),400
        else:
            return jsonify(tone.jsonready)

@bp.route('/_carrier', methods=['POST'])
@login_required
@perm_required('modify_carriers')
def json_set_carrier():
    if not request.json:
       abort(418)
    carrier_data = request.json
    try:
            carrier_id = carrier_data['id']
    except:
        carrier_id = -1

    try:
        Carrier.query.filter(Carrier.id==carrier_id).one()
    except:
        # New carrier
        carrier = Carrier(name=carrier_data['name'], textappend=carrier_data['textappend'], mp3append=carrier_data['mp3append'], amrappend=carrier_data['amrappend'])
    else:
        # Existing tone
        carrier = Carrier.query.filter(Carrier.id==carrier_id).one()
        carrier.textappend = carrier_data['textappend']
        carrier.mp3append =  carrier_data['mp3append']
        carrier.amrappend =  carrier_data['amrappend']
    try:
       db.session.add(carrier)
       db.session.commit()
    except:
       return jsonify({'status': 'Failure'}),451
    else:
       return jsonify({'status': 'Success'}),201

@bp.route('/_carrier', methods=['DELETE'])
@login_required
@perm_required('modify_carriers')
def json_delete_carrier():
    if not request.json['id']:
        abort(400)
    carrier_id = request.json['id']
    try:
         Carrier.query.filter(Carrier.id==carrier_id).delete()
         db.session.commit()
    except:
        return jsonify({'status' : 'Failure'}),400
    else:
        return jsonify({'status': 'Success'}),201




@bp.route('/_tone', methods=['GET'])
@login_required
@perm_required(['modify_users', 'modify_tones'])
def json_get_tone():
    id = request.args.get('id', -1, type=int)
    if id == -1:
        returnable = []
        for x in Tone.query.all():
            returnable.append(x.jsonready)
        return jsonify(returnable)
    else:
        try:
            tone = Tone.query.filter(Tone.id==id).one()
        except:
            return jsonify({'status': 'Failure'}),400
        else:
            return jsonify(tone.jsonready)


@bp.route('/_tone', methods=['DELETE'])
@login_required
@perm_required('modify_tones')
def json_delete_tone():
    if not request.json['id']:
        abort(400)
    tone_id = request.json['id']
    try:
         Tone.query.filter(Tone.id==tone_id).delete()
         db.session.commit()
    except:
        return jsonify({'status' : 'Failure'}),400
    else:
        return jsonify({'status': 'Success'}),201

@bp.route('/_tone', methods=['POST'])
@perm_required('modify_tones')
@login_required
def json_set_tone():
    if not request.json:
       abort(418)
    tone_data = request.json
    try:
            tone_id = tone_data['id']
    except:
        tone_id = -1


    try:
        Tone.query.filter(Tone.id==tone_id).one()
    except:
        # New tone

        for x in range(1,15):
            tone_name = "{}{}".format(tone_data['type'],x)
            try:
                Tone.query.filter(Tone.name==tone_name).one()
            except:
                tone = Tone(name=tone_name, settings=json.dumps(tone_data['settings']), triggers=json.dumps(tone_data['triggers'])  )
                break
            else:
                pass
    else:
        # Existing tone
        tone = Tone.query.filter(Tone.id==tone_id).one()
        tone.settings = json.dumps(tone_data['settings'])
        tone.triggers = json.dumps(tone_data['triggers'])
    try:
       db.session.add(tone)
       db.session.commit()
    except:
       return jsonify({'status': 'Failure'}),451
    else:
       return jsonify({'status': 'Success'}),201


@bp.route('/_user',methods=['GET'])
@login_required
@perm_required('modify_users')
def json_get_user():
    id = request.args.get('id', -1, type=int)
    if id != -1:
        user = User.query.filter(User.id==id).one()
        if user != None:
            u_dict = user.__dict__
            del u_dict['_sa_instance_state']
            del u_dict['password']
            return jsonify(u_dict)
    return jsonify({"Error": "Invalid User ID"})

@bp.route('/_member',methods=['GET'])
@login_required
@perm_required('modify_members')
def json_get_member():
    id = request.args.get('id', -1, type=int)
    if id != -1:
        user = AlertMember.query.filter(AlertMember.id==id).one()
        if user != None:
            u_dict = user.__dict__
            del u_dict['_sa_instance_state']
            carrier = Carrier.query.filter(Carrier.id==u_dict['carrier_id']).one().__dict__
            del carrier['_sa_instance_state']
            u_dict['carrier']=carrier
            del u_dict['carrier_id']
            return jsonify(u_dict)
    return jsonify({"Error": "Invalid User ID"})

@bp.route('/_user', methods=['POST'])
@login_required
@perm_required('modify_users')
def json_set_user():
    if not request.json:
       abort(400)
    user_data = request.json
    try:
        user_id = user_data['id']
    except:
        user_id = -1

    if (User.query.filter(User.id==user_id).count() == 0):
        if (user_data['password'] != user_data['confirmpassword'] ):
            abort(418)
        user = User(username=user_data['username'], email=user_data['email'], perms=json.dumps(user_data['perms']), password=user_data['password'] )
    else:
       user = User.query.filter(User.id==user_id).one()
       user.username = user_data['username']
       user.email = user_data['email']
       user.perms = json.dumps(user_data['perms'])
       if  (user_data['password'] != ''):
        if (user_data['password'] != user_data['confirmpassword']):
           abort(418)
        user.password = user_data['password']
    try:
       db.session.add(user)
       db.session.commit()
    except:
       return jsonify({'status': 'Failure'}),451
    else:
       return jsonify({'status': 'Success'}),201


@bp.route('/_member', methods=['POST'])
@login_required
@perm_required('modify_members')
def json_set_member():
    if not request.json:
       abort(400)
    member_data = request.json
    try:
        member_id = member_data['id']
    except:
        member_id = -1

    if (AlertMember.query.filter(AlertMember.id==member_id).count() == 0):
        try:
            parsednumber = phonenumbers.parse(member_data['phone'], 'US').national_number
        except:
            parsednumber = None
            if len(member_data['phone']) > 0:
                flash("Caution: Phone number not valid or empty")
        member = AlertMember(name=member_data['name'], phone=parsednumber, email=member_data['email'], carrier_id=member_data['carrier_id'], receiveops=json.dumps(member_data['receiveops']) )
    else:
       member = AlertMember.query.filter(AlertMember.id==member_id).one()
       member.name = member_data['name']
       member.email = member_data['email']
       try:
           parsednumber = phonenumbers.parse(member_data['phone'], 'US').national_number
       except:
           parsednumber = None
           if len(member_data['phone']) > 0:
               flash("Caution: Phone number not valid or empty")
       member.phone = parsednumber
       member.carrier_id = member_data['carrier_id']
       member.receiveops = json.dumps(member_data['receiveops'])

    try:
       db.session.add(member)
       db.session.commit()
    except:
       return jsonify({'status': 'Failure'}),451
    else:
       return jsonify({'status': 'Success'}),201


@bp.route('/_user', methods=['DELETE'])
@login_required
@perm_required('modify_users')
def json_delete_user():
    if not request.json['id']:
        abort(400)
    user_id = request.json['id']
    if (user_id == 1):
        abort(418)
    try:
         User.query.filter(User.id==user_id).delete()
         db.session.commit()
    except:
        return jsonify({'status' : 'Failure'}),400
    else:
        return jsonify({'status': 'Success'}),201

@bp.route('/_member', methods=['DELETE'])
@login_required
@perm_required('modify_members')
def json_delete_member():
    if not request.json['id']:
        abort(400)
    member_id = request.json['id']
    try:
         AlertMember.query.filter(AlertMember.id==member_id).delete()
         db.session.commit()
    except:
        return jsonify({'status' : 'Failure'}),400
    else:
        return jsonify({'status': 'Success'}),201


def find_members_by_tone(tone):
    all_members = AlertMember.query.all()
    filtered = filter(lambda x : json.loads(x.receiveops).get(tone) != None , all_members)
    return filtered

def get_file(filename):  # pragma: no cover
    try:
        root_dir = os.path.abspath(os.path.dirname(__file__))
        src = os.path.join(root_dir, filename)
        # Figure out how flask returns static files
        # Tried:
        # - render_template
        # - send_file
        # This should not be so non-obvious
        s = open(src).read().encode('utf-8')
        return  s
    except IOError as exc:
        return str(exc)

def get_file_path(filename):  # pragma: no cover
    try:
        root_dir = os.path.abspath(os.path.dirname(__file__))
        src = os.path.join(root_dir, filename)
        # Figure out how flask returns static files
        # Tried:
        # - render_template
        # - send_file
        # This should not be so non-obvious
        return  src
    except IOError as exc:
        return str(exc)
