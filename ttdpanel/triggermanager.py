import functools
import os.path, os, re
import glob
import time
import threading
from uuid import UUID
import uuid
import simpleaudio as sa
from ttdpanel.alertmanager import get_file_path
from flask.cli import with_appcontext
from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for, current_app, Flask, jsonify
)
from ttdpanel.Models import Triggers, Tone, db
from ttdpanel.admin import login_required
from ttdpanel.admin import perm_required
from flask import abort
import json
import urllib.request
import click
import datetime

bp = Blueprint('triggermanager', __name__)

gpio_queue = {}

@bp.route('/triggers')
@login_required
@perm_required('modify_triggers')
def trigger_manager():
    return render_template('alertmanager/triggers.html', triggers=Triggers.query.all(), triggertypes=expanded_trigger_types())


@bp.route('/_trigger',methods=['GET'])
@login_required
@perm_required(['modify_triggers', 'modify_tones'])
def json_get_trigger():
    id = request.args.get('id', -1, type=int)
    if id != -1:
        try:
            trigger = Triggers.query.filter(Triggers.id==id).one()
            if trigger != None:
                u_dict = trigger.__dict__
                del u_dict['_sa_instance_state']
                return jsonify(u_dict)
        except:
            return jsonify({"Error": "Invalid Trigger ID"})
    else:
        #try:
        triggers = Triggers.query.all()
        returnable = []
        for trig in triggers:
            u_dict = trig.__dict__
            del u_dict['_sa_instance_state']
            returnable.append(u_dict)
        return jsonify( returnable )
        #except:
            #return jsonify({"Error": "Something went wrong"}),400


@bp.route('/_trigger', methods=['POST'])
@login_required
@perm_required('modify_triggers')
def json_set_trigger():
    if not request.json:
       abort(400)
    trigger_data = request.json
    try:
        trigger_id = trigger_data['id']
    except:
        trigger_id = -1

    if (Triggers.query.filter(Triggers.id==trigger_id).count() == 0):
        # Generate random UUID4
        guid = str(uuid.uuid4())
        trigger = Triggers(name=trigger_data['name'], guid=guid, settings=json.dumps(trigger_data['settings']))
    else:
        trigger = Triggers.query.filter(Triggers.id==trigger_id).one()
        trigger.name = trigger_data['name']
        trigger.settings = json.dumps(trigger_data['settings'])
    try:
        db.session.add(trigger)
        db.session.commit()
    except:
       return jsonify({'status': 'Failure'}),451
    else:
       return jsonify({'status': 'Success'}),201

@bp.route('/_trigger', methods=['DELETE'])
@login_required
@perm_required('modify_triggers')
def json_delete_trigger():
    if not request.json['id']:
        abort(400)
    trigger_id = request.json['id']
    try:
         Triggers.query.filter(Triggers.id==trigger_id).delete()
         db.session.commit()
    except:
        return jsonify({'status' : 'Failure'}),400
    else:
        return jsonify({'status': 'Success'}),201



@bp.route('/_triggertypes', methods=['GET'])
@login_required
@perm_required('modify_triggers')
def json_get_trigger_types():
    return jsonify(expanded_trigger_types())

@bp.route('/tr/<arg>')
def execute_trigger(arg):
    if ( is_valid_uuid(arg) ):
        trigger = Triggers.query.filter(Triggers.guid==arg).one()
        if not trigger:
            abort(400)
        trsettings = json.loads(trigger.settings)
        if (('cooldown' in trsettings) and (trsettings['cooldown'] is not None)):
            if (trsettings['duration'] if trsettings['duration'] is not None else 0 ) > 0 :
                cooldownperiod = trsettings['duration'] + trsettings['cooldown']
            else:
                cooldownperiod =  trsettings['cooldown']
        else:
            cooldownperiod = (trsettings['duration'] if trsettings['duration'] is not None else 0 )
        if ((trigger.lastuse is not None) and ('cooldown' in trsettings) and (trsettings['cooldown'] is not None)) and ((datetime.datetime.now() - trigger.lastuse).total_seconds() < cooldownperiod  ):
            return jsonify({'status': 'Cooldown period'}),429
        types = gtrigger_types()
        types[ trsettings['type'] ]['execute'] (trsettings)
        trigger.lastuse = datetime.datetime.now()
        db.session.add(trigger)
        db.session.commit()
        return jsonify({'status': 'Success'}),200

    else:
        try:
            tone = Tone.query.filter( Tone.name.ilike(arg) ).one()
            for trig in json.loads(tone.triggers):
                try:
                    trigger = Triggers.query.filter(Triggers.id==trig).one()
                    execute_trigger(trigger.guid)
                except:
                    pass
            return jsonify({'status': 'Success'}),200
        except:
            abort(400)


@click.command('exec-trig')
@click.argument('guid')
@with_appcontext
def execute_trigger_command(guid):
    db.init_app(current_app)
    current_app.config['SERVER_NAME'] = 'localhost:5000'
    app = current_app
    with app.app_context(), app.test_request_context():
        trigger_url = url_for('triggermanager.execute_trigger', guid=guid, _external=True )
    req = urllib.request.Request(trigger_url)
    with urllib.request.urlopen(req) as response:
        return response.read()





# Define types of triggers with the trigger function, settings and types of expected values
def gtrigger_types():
    types = {'sound': { "execute":functools.partial(triggerSound),"settings": {"file": functools.partial(listAudioFiles) , "duration": functools.partial(int), "cooldown": functools.partial(int)} }}
    try:
        # Dont include GPIO stuff if this isn't an RPi. If you are on an RPi and you don't have the GPIO option listed, try installing this library via the package manager
        import RPi.GPIO as GPIO
        types['gpio'] = { "execute":functools.partial(triggerGPIO),"settings": {"gpio": functools.partial(int), "duration": functools.partial(int), "cooldown": functools.partial(int)} }
    except:
        pass
    return types





# Expand trigger types with a sane format
def expanded_trigger_types():
    temptypes = gtrigger_types()
    types = temptypes
    for t in temptypes:
        for s in temptypes[t]['settings']:
            temp = {}
            temp['type'] = type(temptypes[t]['settings'][s]()).__name__
            if isinstance(temptypes[t]['settings'][s](), (list, tuple) ):
                temp['values']= temptypes[t]['settings'][s]()
            else:
                temp['values'] = None
            types[t]['settings'][s] = temp
        del types[t]['execute']
    return types

def triggerSound(settings):
    if settings['duration'] and settings['duration'] > 0 :
        stoptmr = threading.Thread(target=triggerSoundStopTimer, args=([settings]))
        stoptmr.start()
        threading.Thread(target=triggerSoundRptTimer, args=([settings, stoptmr ])).start()
    else:
        wave_obj = sa.WaveObject.from_wave_file(get_file_path("static/audio/" + settings['file']))
        play_obj = wave_obj.play()

def triggerSoundRptTimer(settings, stoptmr):
    wave_obj = sa.WaveObject.from_wave_file(get_file_path("static/audio/" + settings['file']))
    while stoptmr.isAlive():
        play_obj = wave_obj.play()
        play_obj.wait_done()
    play_obj.stop()

def triggerSoundStopTimer(settings):
    time.sleep(settings['duration'])
    sa.stop_all()

def triggerGPIO(settings):
    global gpio_queue
    try:
        gpio_queue[settings['gpio']] = gpio_queue[settings['gpio']] + 1
    except:
        gpio_queue[settings['gpio']] = 1 
    threading.Thread(target=triggerGPIOChild, args=([settings])).start()
def triggerGPIOChild(settings):
    global gpio_queue
    try:
        import RPi.GPIO as GPIO
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(settings['gpio'], GPIO.OUT)
        GPIO.output(settings['gpio'], GPIO.HIGH)
        if settings['duration'] > 0:
            time.sleep(settings['duration'])
        else:
            time.sleep(1)
        gpio_queue[settings['gpio']] = gpio_queue[settings['gpio']] - 1
        if gpio_queue[settings['gpio']] < 1:
            GPIO.output(settings['gpio'], GPIO.LOW)
            gpio_queue[settings['gpio']] = 0  # Just in case we go negative for some odd reason

    except:
        return 0
    return 1


def listAudioFiles():
    return  [f for f in os.listdir(get_file_path("static/audio/")) if re.match(r'.+\.[wav|mp3]',f,re.IGNORECASE)]

def is_valid_uuid(uuid_to_test, version=4):
    """
    Check if uuid_to_test is a valid UUID.

    Parameters
    ----------
    uuid_to_test : str
    version : {1, 2, 3, 4}

    Returns
    -------
    `True` if uuid_to_test is a valid UUID, otherwise `False`.

    Examples
    --------
    >>> is_valid_uuid('c9bf9e57-1685-4c89-bafb-ff5af830be8a')
    True
    >>> is_valid_uuid('c9bf9e58')
    False
    """
    try:
        uuid_obj = UUID(uuid_to_test, version=version)
    except ValueError:
        return False

    return str(uuid_obj) == uuid_to_test
