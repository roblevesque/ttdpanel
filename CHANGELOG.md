### Version 0.1.2 (October 2019):
* Redesigned user interface with better (hopefully) appearance and responsiveness 

### Version 0.1.1 (October 2019):
* Trigger Support
  - Support for running wget or curl on a generated url to trigger an event. Current *working* event types are: **Playing a sound**
    - This is useful to be executed in a script when TTD detects the tones.
    - Others are planned (like RPI GPIO triggering) soonly
* Signed in user profile editing (email address, password etc)
* Allow for notifications to be sent to a member's email addresses
* Bug fix relating to the way the member addition/edit modal displayed
* Minor improvement in tones.cfg generation code to offset dirty hack to add email addresses in.
* When removing a member you should now stay on the same page or go to the previous one it no longer exists

### Initial Version. Henceforth known as v0.1.0 (August 2019):
* Intial Features include:
  - Serving up tones.cfg for TwoToneDetect
  - Tone management
  - Member management (Who gets alerted on toneouts)
  - Carrier Management (Types of carriers)
