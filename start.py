#! /usr/bin/python


import os
from flask import Flask
import subprocess
from multiprocessing import Process
from waitress import serve
from werkzeug.contrib.fixers import ProxyFix

os.environ['FLASK_APP'] = 'ttdpanel'
os.environ['FLASK_ENV'] = 'production'

#subprocess.check_output(['flask', 'init-db-if-missing'])

from ttdpanel import create_app
app = create_app()
app.wsgi_app = ProxyFix(app.wsgi_app)

if ( app.config['SECRET_KEY'] == 'CHANGEME'):
    raise Exception('Please change "SECRET_KEY" value in instance/config.cfg. THE DEFAULT IS INSECURE!' )

serve(app, host=app.config['LISTEN_ON'], port=app.config['PORT'])
